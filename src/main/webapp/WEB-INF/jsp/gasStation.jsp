<%--
  Created by IntelliJ IDEA.
  User: romaz
  Date: 22.11.2019
  Time: 9:52
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="header.jsp"/>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<div class="gasStation-container">
    <div class="message">
        ${message}
    </div>
</div>

<jsp:include page="footer.jsp"/>
